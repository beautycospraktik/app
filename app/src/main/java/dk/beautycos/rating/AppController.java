package dk.beautycos.rating;

import android.app.Activity;

public abstract class AppController {

    protected static final String SESSION_FILE = "/session.bin";

    protected static boolean mFlagInit = false;

    public static Session _session;

    public static Session Init(Activity _activity) {
        if (mFlagInit) {
            return _session;
        }

        _session = Session.Deserialize(_activity.getFilesDir().toString() + SESSION_FILE);

        if (_session == null) {
            _session = new Session();
        }

        mFlagInit = true;
        return _session;

    }

    public static boolean Save(Activity _activity) {
        Session.Serialize(AppController._session, _activity.getFilesDir().toString() + SESSION_FILE);
        return true;
    }

    public static boolean SessionPurge(Activity _activity) {
        Session.DestroySession(_activity.getFilesDir().toString() + SESSION_FILE);
        _session = new Session();
        return true;
    }
}
