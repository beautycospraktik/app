package dk.beautycos.rating;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Update extends AsyncTask<String, String, String> {

    private static Activity activity;
    private static int VERSION = BuildConfig.VERSION_CODE;
    private static Boolean validated = false;

    public static void CheckUpdate(Activity _activity) {
        new Update().execute("http://app.wehappy.dk/app-release.apk");
        activity = _activity;
    }

    @Override
    protected String doInBackground(String... urls){
        try {
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url("http://app.wehappy.dk/output.json")
                    .get()
                    .build();

            Response response = client.newCall(request).execute();

            JsonParser parser = new JsonParser();
            String jsonString = response.body().string();
            JsonObject jsonElement = parser.parse(jsonString).getAsJsonObject();
            ValidateVersion(jsonElement.get("versionCode").getAsInt());
        } catch (Exception e) {
            Log.e("error", e.getMessage());
        }

        String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/wehappy.apk";
        if (validated) {
            try{
                URL url = new URL(urls[0]);
                URLConnection connection = url.openConnection();
                connection.connect();

                InputStream input = new BufferedInputStream(url.openStream());
                OutputStream output = new FileOutputStream(path);

                byte data[] = new byte[1024];
                int count;
                while ((count = input.read(data)) != -1)
                {
                    output.write(data, 0, count);
                }

                output.flush();
                output.close();
                input.close();
            }catch(Exception e){
                Log.e("error doInBackground", e.getMessage() );
            }
        }

        return path;
    }

    @Override
    protected void onPostExecute(String path) {
        super.onPostExecute(path);
        if (validated) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/wehappy.apk")), "application/vnd.android.package-archive");
            activity.startActivity(intent);
        }
    }

    protected void ValidateVersion(int _version) {
        int currentVersion = VERSION;
        if (VERSION < _version) {
            validated = true;
        }
    }
}