package dk.beautycos.rating;

import android.app.Activity;

import java.io.*;

public class Session implements java.io.Serializable {

    public String mTabletId;
    public String mStoreId;
    public String mName;
    public int mPassword;
    public String mStoreName;
    public int mVersionCode;
    public boolean exist;

    public static void Init(Activity a) {
        Session temp = Deserialize(a.getFilesDir().toString() + "/session.bin");
        if (temp != null) {
            AppController._session = temp;
        }
    }

    public static Boolean Serialize(Session obj, String path) {
        try {
            FileOutputStream sw = new FileOutputStream(path);

            ObjectOutputStream out = new ObjectOutputStream(sw);

            out.writeObject(obj);
            out.close();

            sw.close();

            return true;

        } catch (IOException e) {
            return false;
        }
    }

    public static Session Deserialize(String path) {
        try {
            FileInputStream sr = new FileInputStream(path);

            ObjectInputStream in = new ObjectInputStream(sr);
            Session result = (Session) in.readObject();

            in.close();

            sr.close();

            return result;
        } catch (IOException i) {
            return null;
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    public static boolean DestroySession(String path) {
        try {

            File file = new File(path);

            if (file.exists()) {
                file.delete();
            }

            return true;

        } catch (Exception e) {
            return false;
        }
    }
}
