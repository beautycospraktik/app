package dk.beautycos.rating;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class RatingOffline {
    public static Boolean Serialize(String rate, String path) {
        try {
            FileOutputStream sw = new FileOutputStream(path);

            ObjectOutputStream out = new ObjectOutputStream(sw);

            out.writeObject(rate);
            out.close();

            sw.close();

            return true;

        } catch (IOException e) {
            return false;
        }
    }

    public static String Deserialize(String path) {
        try {
            FileInputStream sr = new FileInputStream(path);

            ObjectInputStream in = new ObjectInputStream(sr);
            String result = (String) in.readObject();

            in.close();

            sr.close();

            return result;
        } catch (IOException i) {
            return null;
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    public static boolean DestroyRate(String path) {
        try {

            File file = new File(path);

            if (file.exists()) {
                file.delete();
            }

            return true;

        } catch (Exception e) {
            return false;
        }
    }
}
