package dk.beautycos.rating;

import android.app.ActionBar;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.util.Iterator;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Tablet extends AsyncTask<String, String, String> implements Iterable {

    public String tabletId;
    public String name;
    public int versionCode;
    public int password;

    public static void UpdateTablet(Session Tablet) {
        Gson g = new Gson();
        Tablet temp = new Tablet();
        temp.tabletId = Tablet.mTabletId;
        temp.name = Tablet.mName;
        temp.versionCode = Tablet.mVersionCode;
        temp.password = Tablet.mPassword;
        String JsonString = g.toJson(temp);
        new Tablet().execute(JsonString);
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, params[0]);
            Request request = new Request.Builder()
                    .url("https://beautycosapi.herokuapp.com/updateTablet")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Cache-Control", "no-cache")
                    .build();

            JsonParser parser = new JsonParser();
            Response response = client.newCall(request).execute();
            String jsonString = response.body().string();
            JsonObject jsonElement = parser.parse(jsonString).getAsJsonObject();
            JsonObject tablet = jsonElement.get("tablet").getAsJsonObject();
            MainActivity.ParseToSession(tablet);
        } catch (IOException e) {
            Log.i("test", e.getMessage());
        }
        return "";
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }

    @NonNull
    @Override
    public Iterator iterator() {
        return null;
    }
}
