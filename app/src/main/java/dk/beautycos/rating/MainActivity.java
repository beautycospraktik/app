package dk.beautycos.rating;

import android.Manifest;
import android.app.Activity;
import android.app.Service;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import butterknife.ButterKnife;

public class MainActivity extends Activity {

    final int ExternalReadStoragePermission = 1337;
    final int ExternalWriteStoragePermission = 3173;

    public WebView myWebView;
    private View mDecorView;
    private static Activity _activity;
    public String deviceId;
    public static View main;

    public InputMethodManager im;

    private static boolean IsSettingsActive;
    private static WebView SettingsView;
    private boolean isKioskEnabled;
    private DevicePolicyManager mDpm;
    private BroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        _activity = this;
        deviceId = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        AppController.Init(_activity);
        mDecorView = getWindow().getDecorView();

        getView();

        ComponentName deviceAdmin = new ComponentName(this, KioskDeviceAdminReceiver.class);
        mDpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        if (!mDpm.isAdminActive(deviceAdmin)) {
            Toast.makeText(this, getString(R.string.not_device_admin), Toast.LENGTH_SHORT).show();
        }

        if (mDpm.isDeviceOwnerApp(getPackageName())) {
            mDpm.setLockTaskPackages(deviceAdmin, new String[]{getPackageName()});
        } else {
            Toast.makeText(this, getString(R.string.not_device_owner), Toast.LENGTH_SHORT).show();
        }

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        im = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);

        OnInit();
    }

    @Override
    public void onBackPressed() {
        if (IsSettingsEnabled()) {
            UnsetSettingsView();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RatingThread.StopThread();
    }

    private void getView () {
        Session status = AppController._session;
        if (status.mStoreId == null) {
            login();
        }else{
            ratingView(false);
        }
    }

    private void ratingView(Boolean _isEnabled) {
        isKioskEnabled = _isEnabled;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                login();
                if (isKioskEnabled) {
                    toggleView(true);
                    startLockTask();
                    hideSystemUI();
                } else {
                    toggleView(false);

                    // Set programatically text
                    SetStrings();

                    showSystemUI();
                    stopLockTask();
                    Permissions();
                }
            }
        });
    }

    private void toggleView(boolean enabled) {
        if (enabled == true) {
            setContentView(R.layout.web_view);
            myWebView = new WebView(this) {
                boolean flag = false;

                @Override
                public boolean onTouchEvent(MotionEvent event) {
                    super.onTouchEvent(event);
                    if (this.flag) {
                        im.toggleSoftInput(0, InputMethodManager.SHOW_IMPLICIT);
                    }else{
                        hideSystemUI();
                    }
                    return true;
                }
            };

            myWebView.setWebViewClient(new WebViewClient());
            myWebView.clearCache(true);
            myWebView.getSettings().setJavaScriptEnabled(true);
            CookieManager.getInstance().setAcceptCookie(true);
            CookieManager.getInstance().setAcceptThirdPartyCookies(myWebView, true);

            //myWebView.loadUrl("https://test.rating.wehappy.dk?info=" + AppController._session.mStoreId + "," + AppController._session.mTabletId);
            myWebView.loadUrl("file:///android_asset/index.html?info=" + AppController._session.mStoreId + "," + AppController._session.mTabletId + "," + AppController._session.mPassword);

            myWebView.addJavascriptInterface(new WebAppInterface(this), "Android");
            setContentView(myWebView);

        } else {
            try {
                setContentView(R.layout.activity_main);
            }
            catch (Exception e) {
                Log.i("test", e.getMessage());
            }

            findViewById(R.id.button_toggle_kiosk).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ratingView(true);
                }
            });

            findViewById(R.id.button_check_update).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Update.CheckUpdate(_activity);
                }
            });

            findViewById(R.id.button_logout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppController.SessionPurge(_activity);
                    getView();
                }
            });

            findViewById(R.id.settings).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SetSettingsView(_activity);
                }
            });
        }
        ButterKnife.bind(this);
    }

    public void login () {
        setContentView(R.layout.web_view);

        myWebView = new WebView(this);
        myWebView.setWebViewClient(new WebViewClient());
        myWebView.clearCache(true);
        myWebView.getSettings().setJavaScriptEnabled(true);
        CookieManager.getInstance().setAcceptCookie(true);
        CookieManager.getInstance().setAcceptThirdPartyCookies(myWebView, true);

        //myWebView.loadUrl("https://test.rating.wehappy.dk/creation?info=" + deviceId);
        myWebView.loadUrl("file:///android_asset/setup.html?info=" + deviceId);

        myWebView.addJavascriptInterface(new WebAppInterface(this), "Android");
        setContentView(myWebView);
    }

    public class WebAppInterface {
        Context mContext;

        WebAppInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void disableKiosk() {
            ratingView(false);
        }

        @JavascriptInterface
        public void login(String tablet) {

            Gson g = new Gson();
            Session t = g.fromJson(tablet, Session.class);

            AppController._session.mStoreId = t.mStoreId;
            AppController._session.mTabletId = deviceId;
            AppController._session.mName = t.mName;
            AppController._session.mPassword = t.mPassword;
            AppController._session.mStoreName = t.mStoreName;
            AppController._session.mVersionCode = BuildConfig.VERSION_CODE;

            /*if (!t.exist) {
                Tablet.UpdateTablet(AppController._session);
            }*/

            AppController.Save(_activity);

            getView();
        }

        @JavascriptInterface
        public void rate(String rate) {
            Rating.AddToQueue(rate);
        }


        @JavascriptInterface
        public void SaveSettings(String settings) {
            Gson g = new Gson();
            Tablet t = g.fromJson(settings, Tablet.class);
            Session temp = new Session();
            temp.mTabletId = AppController._session.mTabletId;
            temp.mPassword = (t.password != 0 ? t.password : AppController._session.mPassword);
            temp.mName = (t.name != null ? t.name : AppController._session.mName);
            temp.mVersionCode = (t.versionCode != 0 ? t.versionCode : AppController._session.mVersionCode);

            Tablet.UpdateTablet(temp);
        }
    }

    private void SetStrings() {
        // Set version number
        Button uButton = findViewById(R.id.button_check_update);
        uButton.setText("v" + BuildConfig.VERSION_NAME);

        // Set info
        TextView storeName = findViewById(R.id.storeName);
        TextView tabletName = findViewById(R.id.tabletName);

        storeName.setText(AppController._session.mStoreName);
        tabletName.setText(AppController._session.mName);
    }

    private void OnInit() {
        Rating.Init(this);
        // NetworkChangeReceiver.Init(this);
        Session.Init(this);
    }

    private void Permissions() {
        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                // Explain to the user why we need to read the contacts
            }
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    ExternalReadStoragePermission);
            return;
        }
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Explain to the user why we need to read the contacts
            }
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    ExternalWriteStoragePermission);
            return;
        }
    }

    private void SetSettingsView(Activity a) {
        a.setContentView(R.layout.web_view);
        if (SettingsView == null) {
            SettingsView = new WebView(a);
            SettingsView.setWebViewClient(new WebViewClient());
            SettingsView.clearCache(true);
            SettingsView.getSettings().setJavaScriptEnabled(true);


            SettingsView.loadUrl("file:///android_asset/settings.html?info="+ AppController._session.mStoreName +","+ AppController._session.mName +","+ AppController._session.mPassword +","+ AppController._session.mTabletId);
            SettingsView.addJavascriptInterface(new WebAppInterface(this), "Android");
        }
        a.setContentView(SettingsView);
        IsSettingsActive = !IsSettingsActive;
    }

    private void UnsetSettingsView() {
        ratingView(false);
        IsSettingsActive = !IsSettingsActive;
    }

    private boolean IsSettingsEnabled() {
        return IsSettingsActive;
    }

    public static void ParseToSession(JsonObject response) {
        AppController._session.mPassword = response.get("password").getAsInt();
        AppController._session.mName = response.get("name").getAsString();

        AppController.Save(_activity);
    }

    private void hideSystemUI() {
        mDecorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    private void showSystemUI() {
        mDecorView.setSystemUiVisibility(
                View.VISIBLE
        );
    }
}
