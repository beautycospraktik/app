package dk.beautycos.rating;

import android.support.annotation.NonNull;

import java.io.Serializable;

public class Rate implements Comparable<Rate>, Serializable {
    public int rate;
    public long date;
    public String storeId;
    public String tabletId;
    public negative negative;
    public positive positive;

    @Override
    public int compareTo(@NonNull Rate rate) {
        return 0;
    }

    public class negative {
        public int value;
    }
    public class positive {
        public int value;
    }
}
