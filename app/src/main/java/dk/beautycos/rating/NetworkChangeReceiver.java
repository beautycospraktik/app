package dk.beautycos.rating;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkChangeReceiver extends BroadcastReceiver {

    protected static boolean connected = false;

    public static void Init(Activity a) {
        IsOnline(a);
        Rating.CheckLocal();
    }

    public static boolean GetConnectionState() {
        return connected;
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        IsOnline(context);
        if (connected) {
            Rating.CheckLocal();
        }
    }

    private static void IsOnline (Context c) {
        ConnectivityManager connMgr = (ConnectivityManager) c
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null) {
            connected = networkInfo.isConnected();
        } else {
            connected = false;
        }
    }
}
