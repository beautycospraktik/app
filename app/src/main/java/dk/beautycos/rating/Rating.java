package dk.beautycos.rating;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.util.PriorityQueue;
import java.util.Queue;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Rating {
    protected static Queue<Rate> Rates;
    protected static Object lock;

    protected static final String TEMP_NAME = "/rate-";
    protected static Activity MainActivity;

    public static void Init(Activity a) {
        Rates = new PriorityQueue<Rate>();
        lock = new Object();
        MainActivity = a;
        RatingThread.StartThread();
        String path = MainActivity.getFilesDir().toString() + "/rates/";
        File dir = new File(path);
        if (!dir.exists()) {
            dir.mkdir();
        }
    }
    public static void AddToQueue(String rate) {
        Rate _rate = ConvertRate(rate);
        synchronized (lock) {
            Rates.offer(_rate);
        }

        if (!NetworkChangeReceiver.GetConnectionState()) {
            RatingOffline.Serialize(rate, MainActivity.getFilesDir().toString() + "/rates/" + TEMP_NAME + _rate.date + ".bin");
        } else {
            RatingThread.StartProcess();
        }
    }

    public static void DeQueue() {
        Rate _rate = Rates.remove();
        SendRate(_rate);
    }

    public static Rate ConvertRate(String rate) {
        Gson g = new Gson();
        Rate r = g.fromJson(rate, Rate.class);
        return r;
    }

    public static boolean IsQueueEmpty() {
        return Rates.size() != 0;
    }

    public static void CheckLocal() {
        String path = MainActivity.getFilesDir().toString() + "/rates/";
        String[] list = new File(path).list();
        if (NetworkChangeReceiver.GetConnectionState()) {
            for (String s:list) {
                String _rate = RatingOffline.Deserialize(path + s.toString());
                AddToQueue(_rate);
                RatingOffline.DestroyRate(path + s.toString());
            }
            RatingThread.StartProcess();
        }
    }

    protected static void SendRate(Rate rate) {
        Gson g = new Gson();
        String _rate = g.toJson(rate, Rate.class);
        try {
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, _rate);
            Request request = new Request.Builder()
                    .url("https://beautycosapi.herokuapp.com/rate")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Cache-Control", "no-cache")
                    .build();

            Response response = client.newCall(request).execute();
        } catch (IOException e) {
            Log.i("test", e.getMessage());
        }
    }
}
