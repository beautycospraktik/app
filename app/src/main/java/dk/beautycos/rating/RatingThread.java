package dk.beautycos.rating;

public class RatingThread extends Thread {
    protected static RatingThread _ratingThread;
    protected static boolean running;
    protected static Object lock;

    public void run() {
        while (running) {
            while (Rating.IsQueueEmpty()) {
                Rating.DeQueue();
            }
            try {
                synchronized (_ratingThread) {
                    _ratingThread.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public static void StartThread() {
        if (_ratingThread == null) {
            lock = new Object();
            running = true;
            _ratingThread = new RatingThread();
            _ratingThread.start();
        }
    }
    public static void StopThread() {
        running = false;
    }
    public static void StartProcess() {
        synchronized (_ratingThread) {
            _ratingThread.notify();
        }
    }
}
